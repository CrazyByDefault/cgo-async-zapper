all: go test

go:
	go build -buildmode=c-archive -o dist/ log.go

c:
	gcc -pthread test/test.c dist/log.a -o dist/test.out

test: c
	./dist/test.out
	cat /tmp/test.log

clean:
	rm -rf ./dist/*