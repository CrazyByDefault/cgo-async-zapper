# CGo Wrapper for Zap in golang

The purpose of this repo is to quickly demonstrate how C logs can be exported to an asynchronous golang log collector. We're using Uber's [Zap](https://github.com/uber-go/zap) as a fast, unbuffered log mechanism to write the logs, and exporting async funcs to call from C with data.

## Run it

`make all` will build the go lib with `buildmode=c-archive`, and build test.c with it. It should then run test.c, and show the output of `/tmp/test.log` to which we have successfully logged via a golang function.