package main

import (
	"C"

	"os"

	"go.uber.org/zap"
)

//export AsyncLogInfo
func AsyncLogInfo(msg *C.char) {
	// defer LogInfo(C.GoString(msg))
	// go LogInfo(C.GoString(msg))
	// ^ learn what these do

	LogInfo(C.GoString(msg))
}

func LogInfo(msg string) {
	// create tmp file at /tmp/test.log
	if _, err := os.Stat("/tmp/test.log"); os.IsNotExist(err) {
		os.Create("/tmp/test.log")
	}

	log, _ := newLogger("/tmp/test.log")
	defer log.Sync()
	log.Info(msg)
}

func newLogger(logpath string) (*zap.Logger, error) {
	cfg := zap.NewProductionConfig()
	cfg.OutputPaths = []string{
		logpath,
	}
	return cfg.Build()
}

func main() {
	LogInfo("hello")
	go LogInfo("hello async")
}
